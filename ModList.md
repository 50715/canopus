# List of used mods

Some mods have been slightly modified.

Most of the changes are sacred with translation.

I completely removed everything related to translation via intlib and transferred everything to the standard minetest translation functions.

|Source|Catalog|
|-----|-----|
|[minetest_game](https://github.com/minetest/minetest_game.git)|mods/minetest|
|[mobs_npc](https://notabug.org/TenPlus1/mobs_npc.git)|mods/mobs/mobs_npc|
|[mob_horse](https://notabug.org/TenPlus1/mob_horse.git)|mods/mobs/mobs_horse|
|[mobs_water](https://notabug.org/TenPlus1/mobs_water.git)|mods/mobs/mobs_water|
|[mobs_monster](https://notabug.org/TenPlus1/mobs_monster.git)|mods/mobs/mobs_monster|
|[mobs_animal](https://notabug.org/TenPlus1/mobs_animal.git)|mods/mobs/mobs_animal|
|[mobs_redo](https://notabug.org/TenPlus1/mobs_redo.git)|mods/mobs/mobs_redo|
|[travelnet](https://github.com/Sokomine/travelnet.git)|mods/tech/travelnet|
|[streetlights](https://gitlab.com/4w/streetlights.git)|mods/tech/streetlights|
|[ilights](https://gitlab.com/VanessaE/ilights.git)|mods/tech/ilights|
|[destroyer](https://bitbucket.org/50715/destroyer.git)|mods/tech/destroyer|
|[digilines](https://github.com/minetest-mods/digilines.git)|mods/tech/digilines|
|[hopper](https://github.com/minetest-mods/hopper.git)|mods/tech/hopper|
|[hunger_ng](https://gitlab.com/4w/hunger_ng.git)|mods/player/hunger_ng|
|[craftguide](https://github.com/minetest-mods/craftguide.git)|mods/player/craftguide|
|[3d_armor](https://github.com/minetest-mods/3d_armor.git)|mods/player/3d_armor|
|[minetest-toolranks](https://github.com/lisacvuk/minetest-toolranks.git)|mods/player/minetest-toolranks|
|[minetest-illumination](https://notabug.org/Piezo_/minetest-illumination.git)|mods/player/minetest-illumination|
|[handholds_redo](https://github.com/t-affeldt/handholds_redo.git)|mods/player/handholds_redo|
|[unifieddyes](https://gitlab.com/VanessaE/unifieddyes.git)|mods/world/unifieddyes|
|[moreores](https://github.com/minetest-mods/moreores.git)|mods/world/moreores|
|[basic_materials](https://gitlab.com/VanessaE/basic_materials.git)|mods/world/basic_materials|
|[hardtrees_redo](https://bitbucket.org/50715/hardtrees_redo.git)|mods/world/hardtrees_redo|
|[moreblocks](https://github.com/minetest-mods/moreblocks.git)|mods/world/moreblocks|
|[mesecons](https://github.com/minetest-mods/mesecons.git)|mods/mesecons|
|[bonemeal](https://notabug.org/TenPlus1/bonemeal.git)|mods/farming/bonemeal|
|[farming](https://notabug.org/TenPlus1/farming.git)|mods/farming/farming|
|[flowerpot](https://github.com/minetest-mods/flowerpot.git)|mods/farming/flowerpot|
