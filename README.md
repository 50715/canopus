# Canopus Game

This game is based on the original minetest. It reflects the author's vision of what the minetest should be.

## Installation

- Unzip the archive, rename the folder to canopus and
place it in .. minetest/games/

- GNU/Linux: If you use a system-wide installation place
    it in ~/.minetest/games/.

The Minetest engine can be found at [GitHub](https://github.com/minetest/minetest).

For further information or help, see:  
https://wiki.minetest.net/Installing_Mods

## Compatibility

Minetest 5.4.0

## Licensing

See `LICENSE.txt`
