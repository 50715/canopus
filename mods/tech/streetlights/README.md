This modpack combines three mods to tegister poles and lamps with a powerful API.

![Some streetlights](screenshot.png)

The [Streetlights API](https://gitlab.com/4w/streetlights/tree/master/streetlights_api) allows registering poles and lamps and that provides the Mesecon signal receiver that is used to forward the signal to the poles/lamps.

Then the modpack also contains the [Streetlights Poles](https://gitlab.com/4w/streetlights/tree/master/streetlights_poles) mod that utilizes the *Streetlights API* to register a variety of conductive poles that can be used to transfer the signal from the receiver to the lamps.

With the [Streetlights Lamps](https://gitlab.com/4w/streetlights/tree/master/streetlights_lamps) mod some simple lamps are registered that can be used in combination with the poles to light up the environment.

Using the API by yourself all you need to do is downloading the *Streetlights API* mod, depend on it and register all the poles and lamps you like. The two other mods are only there to show what the API does – or as a simple way to have streetlights in your game.
