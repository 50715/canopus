The *Streetlights API* mod is a helper mod for adding streetlights.

# Functionality
The API allows adding poles and “hidden poles” as well as lamps. The poles are vertically conductive and transfer a Mesecons signal to the lamp. Hidden Poles do the same but are disguised as normal nodes.

The poles and hidden poles connect to a receiver and the receiver has to be directly or indirectly connected to a Mesecons signal source to turn the lamps on.

When registering poles and lamps using the API as described below the mod automatically registers the nodes in “on” and “off” state as well as adding all the necessary node configuration in order to work as described.

# Crafting a receiver
The receiver is needed for connecting the poles. It basically transfers the horizontal signal to a vertical one. The receiver is made of 7 stone nodes (`default:stone`) one vertical Mesecon wire and one regular Mesecon wire.

![Crafting the receiver](screenshots/crafting_the_receiver.png)

You need one receiver for each lamp. See placement section below for more details on how to use it.

# Using the API
The API is accessible via the global `streetlights_api` table and contains two functions. One registers a pole and the other registers a lamp.

When registering those things the API automatically prefixes the resulting nodes with the name of the mod that calls the API function.

## Register a pole
Poles transfer the Mesecons signal and are registered using the `register_pole` API function. The function takes two parameters where the second parameter indicates that the pole is a “hidden pole” (i.e. looks like the base node instead of “made of” the base node).

```Lua
-- Register a normal pole
--
-- The crafting recipe for normal poles needs three of the defined base nodes
-- and one vertical Mesecon wire.
--
--   B _ _
--   B M _
--   B _ _
--
-- B is the base node and M is the vertical Mesecon wire. This returns 3 poles.
-- The poles can connect to a receiver or other poles and can be used to
-- connect a lamp to. All poles connect vertically only.
streetlights_api.register_pole('default:fence_wood')

-- Register a hidden pole
--
-- The crafting recipe for the hidden pole requires two of the base material
-- and one vertical Mesecon wire. The hidden poles work the same as normal
-- poles but look like the base node.
--
--   B _ _
--   M _ _
--   B _ _
--
-- B is the base node, and M is the vertical Mesecon wire. This returns 2
-- hidden poles.
streetlights_api.register_pole('default:sand', true)
```

It’s advised to use “simple” nodes as poles. The top and bottom tiles (textures) of poles are modified. Please don’t use animated textures or advanced texture configurations. Those are currently not supported.

## Register lamps
Registering lamps is a little bit more complex than registering poles but it allows some more customization.

```Lua
streetlights_api.register_lamp('short_id', {
    name = 'Lamp Name',
    crafting = {
        yield = 9,
        recipe = minetesst_recipe_definition
    },
    design = {
        sounds = SimpleSoundSpec,
        nodebox = minetest_fixed_nodebox_definition,
        light_level = LIGHT_MAX,
        off_tiles = minetest_tiles_definition,
        on_tiles = minetest_tiles_definition
    }
})
```

This registers a lamp. The example returns 9 lamps, but as you can see it is up to you. Let’s go through the options.

### Parameters
The first parameter is a short ID part of the lamp. The lamp’s ID will be constructed like this: `mod_name:short_id_STATE` where `STATE` is either `on` or `off`. (The `on` version is hidden from creative inventories). The second parameter is the definition table.

The definition table contains the `name` of the lamp as shown in the hover tool-tip in the inventories - and two sections. All of the definition has to be set in order to register the lamp. If anything is missing the lamp won’t be registered.

#### Crafting
The `crafting` section defines the amount of lamps that will be returned when crafting it (`yield`) as well as the `recipe`. This recipe is a regular Minetest recipe definition. See [the Minetest dev wiki](http://dev.minetest.net/register_craft) for details on that.

#### Design
This sets the design for the lamp.

* `sounds` takes a [SimpleSoundSpec](http://dev.minetest.net/SimpleSoundSpec) (either a custom one or one of the defaults).
* `nodebox` defines the nodebox to be used. See the [second example here](http://dev.minetest.net/Node_boxes) but not the whole thing but only the value of `fixed`.
* `light_level` defines the amount of light emitted by the lamp. It ranges between 0 and the Minetest built-in `LIGHT_MAX`.
* `STATE_tiles` defines the textures for the lamp in the specific states (`on` and `off`). This is 1:1 the same as you would use for registering a node using the Minetest API for that.

# Placement in the world
Basically you place the receiver on the ground and connect a Mesecons signal on ground level to it, then you attach one or more poles to your liking and place a lamp on top of the pole.

![Placement example](screenshots/placement_example.png)

The hidden poles from the reference implementation mod `streetlights_poles` which is provided with the `streetlights` modpack can be used to hide the connection from the Mesecon wiring below the surface. So only the pole and the lamp are visible.

![Hidden placement](screenshots/hidden_placement.png)

Only a small connector indicates that there is a pole hidden in the node.

![Hidden pole detail](screenshots/hidden_pole_detail.png)

Poles and hidden poles can be mixed and matched as needed.
