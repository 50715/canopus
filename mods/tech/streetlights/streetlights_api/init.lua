-- Global table
streetlights_api = { translator = minetest.get_translator('streetlights_api') }

-- Functionality
local items = minetest.get_modpath('streetlights_api')..DIR_DELIM..'items'
dofile(items..DIR_DELIM..'receiver.lua')
dofile(items..DIR_DELIM..'register_pole.lua')
dofile(items..DIR_DELIM..'register_lamp.lua')
