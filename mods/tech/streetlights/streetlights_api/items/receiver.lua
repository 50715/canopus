local S = streetlights_api.translator


-- Generates the Mesecon Signal Receiver tiles
--
-- This function generates the receiver textures based on the provided status
-- value and returns them.
--
-- @param status Either `off` or `on` to indicate what state tiles are to be
--               Returned by the function
-- @return table The tile definition for the given state
local generate_receiver_tiles = function (status)
    local base = 'default_stone.png'
    local planes = 'streetlights_api_receiver_'..status..'_planes.png'
    local sides = 'streetlights_api_receiver_'..status..'_sides.png'
    local connector = 'streetlights_api_connector_'..status..'.png'
    local connector_disabled = 'streetlights_api_connector_disabled.png'
    local sides_addition = 'streetlights_api_receiver_sides_addition.png'

    -- Alter and/or add additions
    base = '('..base..'^[colorize:#000000ff:72)'
    connector = '('..connector..'^[opacity:196)'
    sides = '('..sides..'^[opacity:128)'
    planes = '('..planes..'^[opacity:128)'
    sides_addition = '('..sides_addition..'^[opacity:128)'

    return {
        '('..base..'^'..planes..')^'..connector, -- top
        '('..base..'^'..planes..')^'..connector_disabled, -- bottom
        '('..base..'^'..sides..')^'..sides_addition -- sides
    }
end


-- Conduction rules
local conduction_rules = {
    {x=1, y=0, z=0},
    {x=-1, y=0, z=0},
    {x=0, y=0, z=1},
    {x=0, y=0, z=-1},
    {x=0, y=1, z=0}
}


-- Unified definition
local definition = {
    description = S('Mesecon Signal Receiver'),
    is_ground_content = false,
    sounds = minetest.registered_nodes['default:stone'].sounds,
    groups = { dig_immediate = 2 },
    mesecons = { conductor = { rules = conduction_rules } }
}


-- Register receiver in OFF state
local definition_off = table.copy(definition)
definition_off['tiles'] = generate_receiver_tiles('off')
definition_off.mesecons.conductor['state'] = mesecon.state.off
definition_off.mesecons.conductor['onstate'] = 'streetlights_api:receiver_on'
minetest.register_node('streetlights_api:receiver_off', definition_off)


-- Register receiver in ON state
local definition_on = table.copy(definition)
definition_on['tiles'] = generate_receiver_tiles('on')
definition_on['light_source'] = 2
definition_on.groups['not_in_creative_inventory'] = 1
definition_on['drop'] = 'streetlights_api:receiver_off'
definition_on.mesecons.conductor['state'] = mesecon.state.on
definition_on.mesecons.conductor['offstate'] = 'streetlights_api:receiver_off'
minetest.register_node('streetlights_api:receiver_on', definition_on)


-- Register receiver (OFF) crafting recipe
minetest.register_craft({
    output = 'streetlights_api:receiver_off 7',
    recipe = {
        { 'default:stone', 'default:stone',                   'default:stone' },
        { 'default:stone', 'mesecons_extrawires:vertical_off','default:stone' },
        { 'default:stone', 'mesecons:wire_00000000_off',      'default:stone' }
    }
})
