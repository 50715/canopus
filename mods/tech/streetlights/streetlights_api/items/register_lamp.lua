-- Validate the definition
--
-- Checks the definition for invalidity by simply comparing the given structure
-- against a set of `if` instructions.
--
-- @param definition The definition table to check
-- @return bool      Wither or not the definition is invalid
local is_invalid = function (definition)
    if not definition then return true end

    if definition.crafting then
        if not definition.crafting.recipe then return true end
        if not definition.crafting.yield then return true end
    else
        return true
    end

    if definition.design then
        if not definition.design.sounds then return true end
        if not definition.design.nodebox then return true end
        if not definition.design.light_level then return true end
        if not definition.design.off_tiles then return true end
        if not definition.design.on_tiles then return true end
    else
        return true
    end

    return false
end


-- Register a lamp
--
-- The `id_part` has to be the part of the ID you want to set. Since the part
-- gets properly prefixed and suffixed a mod prefix is not necessary.
--
-- This function automatically registers both of the lamps (on and off) and
-- makes them fully functional.
--
-- The definition table has to look like this:
--
-- {
--   name = "Human-readable name of the lamp",
--   crafting = {      -- Crafting-related definition
--     yield = 5,      -- How many lamps are crafted at once
--     recipe = {...}  -- The recipe table as per Minetest definition
--   },
--   design = {              -- Design of the lamp
--     sounds = {...},       -- A SimpleSoundSpec as per Minetest definition
--     nodebox = {...},      -- A fixed nodebox as per Minetest definition
--     light_level = {...},  -- Light level between LIGHT_MAX and 0
--     off_tiles = {...},    -- The tiles table for OFF state
--     on_tiles = {...}      -- The tiles table for ON state
--   },
-- }
--
-- For SimpleSoundSpec see <http://dev.minetest.net/SimpleSoundSpec>.
--
-- The nodebox ddefinition is always fixed. Enter the table of boxes only when
-- defining it. See <http://dev.minetest.net/Node_boxes>.
--
-- @param id_part    The part of the ID as described
-- @param definition The lamp definition as described
-- @return void
streetlights_api.register_lamp = function (id_part, definition)
    local current_mod = minetest.get_current_modname()
    local id = current_mod..':'..id_part

    if is_invalid(definition) then
        minetest.log('error', 'Can’t define '..id_part..'. See documentation')
        return
    end

    -- Unified register table
    local register = {
        description = definition.name,
        is_ground_content = false,
        paramtype = 'light',
        drawtype = 'nodebox',
        tiles = {},
        sounds = definition.design.sounds,
        node_box = { type = 'fixed', fixed = definition.design.nodebox },
        groups = { dig_immediate = 2 },
        mesecons = {effector={rules={{x=0,y=1,z=0},{x=0,y=-1,z=0}}}},
    }

    -- Register lamp in OFF state
    local register_off = table.copy(register)
    register_off.tiles = definition.design.off_tiles
    register_off.drop = id..'_off'
    register_off.mesecons.effector['action_on'] = function (pos)
        minetest.set_node(pos, { name = id..'_on' })
    end
    minetest.register_node(id..'_off', register_off)

    -- Register lamp in ON state
    local register_on = table.copy(register)
    register_on.tiles = definition.design.on_tiles
    register_on['light_source'] = definition.design.light_level
    register_on.groups['not_in_creative_inventory'] = 1
    register_on.drop = id..'_off'
    register_on.mesecons.effector['action_off'] = function (pos)
        minetest.set_node(pos, { name = id..'_off' })
    end
    minetest.register_node(id..'_on', register_on)

    minetest.register_craft({
        output = id..'_off '..definition.crafting.yield,
        recipe = definition.crafting.recipe
    })
end
