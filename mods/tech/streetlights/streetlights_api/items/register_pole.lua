local S = streetlights_api.translator


-- Adds connectors to the top and bottom tiles
--
-- This function adds the visible connectors to the poles. It extends the tile
-- definition table to three entries (three entries = top, bottom, sides) if
-- necessary - otherwise the existing entries are used. Then it alters the top
-- and bottom tile to have the connector.
--
-- This currently only works with non-animated normal texture definitions.
--
-- @param tiles  The tile definition to alter
-- @param state  The state (`on` or `off`) of the connector
-- @return table The altered tile definition
local add_connector_to_tiles = function (tiles, state)
    local top = tiles[1]
    local bottom = tiles[1] or tiles[2]

    return {
        '(('..top..')^streetlights_api_connector_'..state..'.png)',
        '(('..bottom..')^streetlights_api_connector_'..state..'.png)',
        tiles[3] or tiles[1],
        tiles[4],
        tiles[5],
        tiles[6]
    }
end


-- Registers a pole
--
-- For registering a pole the base node ID has to be provided as first
-- parameter. The function then registers the pole in the ON and OFF state and
-- adds the necessary Mesecons-related definitions.
--
-- When `hidden` results in a boolean `true` then the function creates a hidden
-- pole that looks like the `base` ID.
--
--   Normal pole                |  Hidden pole (`hidden == true`)
--  ----------------------------+-----------------------------------
--   mod_name:pole_base_id_on   |  mod_name:pole_base_id_hidden_on
--   mod_name:pole_base_id_off  |  mod_name:pole_base_id_hidden_off
--
-- `mod_name` is – of course – the name of the mod that registers the pole.
-- The `base_id` is the ID of the node that is used for the pole but the `:`
-- is replaced by `_`. `_on` and `_off` are the different versions for the
-- different Mesecons states.
--
-- @param base   The ID of the base node
-- @param hidden Optional boolean `true` if the pole should be hidden
-- @return void
streetlights_api.register_pole = function (base, hidden)
    -- Bail out early when base does not exist
    if minetest.registered_nodes[base] == nil then
        minetest.log('error', 'Can’t register pole: non-existent '..base)
        return
    end

    -- Get base definition tables and stuff
    local current_modname = minetest.get_current_modname()
    local basedef = table.copy(minetest.registered_nodes[base])
    local node_id = current_modname..':pole_'..base:gsub(':', '_')
    local definition = table.copy(basedef)

    -- Set definition options depending on hidden or not hidden
    if hidden then
        node_id = node_id..'_hidden'
        definition.description = S('Hidden Lamp Pole (@1)', basedef.description)
    else
        definition.description = S('Lamp Pole (@1)', basedef.description)
        definition['paramtype'] = 'light'
        definition['drawtype'] = 'nodebox'
        definition['node_box'] = {
            type = 'fixed',
            fixed = { -0.125, -0.5, -0.125, 0.125, 0.5, 0.125 }
        }
    end

    -- Clear/Change general definition
    definition.name = nil
    definition.mod_origin = nil
    definition.inventory_image = nil
    definition.wield_image = nil
    definition.groups['dig_immediate'] = 2
    definition.is_ground_content = false
    definition['mesecons'] = {
        conductor = {
            rules = { {x=0, y=1, z=0}, {x=0, y=-1, z=0} },
        }
    }

    -- Register pole in OFF state
    local definition_off = table.copy(definition)
    definition_off.tiles = add_connector_to_tiles(definition.tiles, 'off')
    definition_off.mesecons.conductor['state'] = mesecon.state.off
    definition_off.mesecons.conductor['onstate'] = node_id..'_on'
    minetest.register_node(node_id..'_off', definition_off)

    -- Register pole in ON state
    local definition_on = table.copy(definition)
    definition_on.tiles = add_connector_to_tiles(definition.tiles, 'on')
    definition_on.drop = node_id..'_off'
    definition_on.groups['not_in_creative_inventory'] = 1
    definition_on.mesecons.conductor['state'] = mesecon.state.on
    definition_on.mesecons.conductor['offstate'] = node_id..'_off'
    minetest.register_node(node_id..'_on', definition_on)

    -- Register craft basing on hidden or not
    if hidden then
        minetest.register_craft({
            output = node_id..'_off 2',
            recipe = {
                { base,                               '', '' },
                { 'mesecons_extrawires:vertical_off', '', '' },
                { base,                               '', '' }
            }
        })
    else
        minetest.register_craft({
            output = node_id..'_off 3',
            recipe = {
                { base, '',                                 '' },
                { base, 'mesecons_extrawires:vertical_off', '' },
                { base, '',                                 '' }
            }
        })
    end

    -- Log registration of new pole definition if enabled
    if minetest.is_yes(minetest.settings:get('streetlights_api_log')) then
        minetest.log('action', ('+s +m registers “+n”'):gsub('+.', {
            ['+s'] = '[streetlights_api]',
            ['+m'] = current_modname,
            ['+n'] = definition.description
        }))
    end
end
