This mod adds some toggleable lamps for the *Streetlights API*.

![Screenshot of the lamps](screenshot.png)

The lamps are the default Mese lamps (full node lamp and post light) and one shaped Mese lamp and they can be connected to a pole registered through the [Streetlights API mod](https://gitlab.com/4w/streetlights/tree/master/streetlights_api) or its receiver. The mod should be seen as reference implementation or an easy way to see what the API does without having to create an own mod using the API.
