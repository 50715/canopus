register_lamp = streetlights_api.register_lamp
S = minetest.get_translator('streetlights_lamps')


-- Simple lamp
register_lamp('simple', {
    name = S('Simple Lamp'),
    crafting = {
        yield = 9,
        recipe = {
            {'default:glass', 'default:glass',              'default:glass'},
            {'default:glass', 'default:meselamp',           'default:glass'},
            {'default:glass', 'mesecons:wire_00000000_off', 'default:glass'}
        }
    },
    design = {
        sounds = default.node_sound_glass_defaults(),
        nodebox = { {-0.25, -0.5, -0.25, 0.25, 0.5, 0.25} },
        light_level = LIGHT_MAX-5,
        off_tiles = { 'default_meselamp.png' },
        on_tiles = { 'default_meselamp.png' }
    },
})


-- Toggleable Mese post light
local mpld = table.copy(minetest.registered_nodes['default:mese_post_light'])
register_lamp('mese_post_light', {
    name = S('Toggleable Mese Post Light'),
    crafting = {
        yield = 1,
        recipe = {
            {'default:mese_post_light', 'mesecons:wire_00000000_off', ''},
            {'',                        '',                           ''},
            {'',                        '',                           ''}
        }
    },
    design = {
        sounds = mpld.sounds,
        nodebox = mpld.node_box.fixed,
        light_level = mpld.light_source,
        off_tiles = mpld.tiles,
        on_tiles = mpld.tiles
    },
})


-- Toggleable Mese lamp
local mld = table.copy(minetest.registered_nodes['default:meselamp'])
register_lamp('mese_lamp', {
    name = S('Toggleable Mese Lamp'),
    crafting = {
        yield = 1,
        recipe = {
            {'default:meselamp', 'mesecons:wire_00000000_off', ''},
            {'',                 '',                           ''},
            {'',                 '',                           ''}
        }
    },
    design = {
        sounds = mld.sounds,
        nodebox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
        light_level = mld.light_source,
        off_tiles = mld.tiles,
        on_tiles = mld.tiles
    },
})

