This mod adds some conductive poles for the *Streetlights API*.

![Screenshot of some poles](screenshot.png)

The poles come in various materials and can be connected to a receiver from the [Streetlights API mod](https://gitlab.com/4w/streetlights/tree/master/streetlights_api) and the mod should be seen as reference implementation or an easy way to see what the API does without having to create an own mod using the API.
