register_pole = streetlights_api.register_pole

local both = {
    'default:cobble',
    'default:fence_acacia_wood',
    'default:fence_aspen_wood',
    'default:fence_junglewood',
    'default:fence_pine_wood',
    'default:fence_wood',
    'default:stone'
}

local hidden = {
    'default:cobble',
    'default:desert_cobble',
    'default:desert_sandstone',
    'default:desert_sandstone_block',
    'default:desert_sandstone_brick',
--    'default:desert_stone from',  -- errors with start game
    'default:desert_stone_block',
    'default:desert_stonebrick',
    'default:dirt',
    'default:dirt_with_grass',
    'default:mossycobble',
    'default:sand',
    'default:sandstone',
    'default:sandstone_block',
    'default:sandstonebrick',
    'default:silver_sandstone',
    'default:silver_sandstone_block',
    'default:silver_sandstone_brick',
    'default:stone',
    'default:stone_block',
    'default:stonebrick',
    'default:tree',
    'walls:cobble',
    'walls:desertcobble',
    'walls:mossycobble'
}

for _,base in pairs(both) do
    register_pole(base)
    register_pole(base, true)
end

for _,base in pairs(hidden) do
    register_pole(base, true)
end


