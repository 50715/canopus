# Hard Trees Redo [hardtrees_redo]

## License

LGPL v3 (see LICENSE.md)

## Original mod

[hardtrees](https://github.com/octacian/hardtrees.git)

## Description

hardtrees redo is a mod with a very simple concept. Trees are made harder, meaning you cannot punch them with your fists.

Instead, you will need to collect some leaves and get sticks out of them. Then you can get boards from them and create the necessary tools.

The mod uses overrides to make trees unbreakable by hand.

Leaves are also modified to act as ladders, so you can climb through the tops of trees.

## Receipt sticks

|Craft table|||
|----|----|----|
|||any leaves|
||any leaves||
|any leaves|||

## Mod Support

hardtrees redo supports all trees within the moretrees and default mod as of July 7th, 2016. To add support for more trees, use the code below modified to fit the new tree and save it in hardtrees/override.lua.

```lua
-- override tree node
hardtrees_redo.override.tree("modname:treename")
-- override leaves
hardtrees_redo.override.leaf("modname:leavesname")
```

`treename` is the name of the tree node so that it can be overrided to not be `oddly_breakable_by_hand`. The `leavesname` is the name of the leaves node which is modified.

### Installation

Download a ZIP of the mod from bitbucket and unzip it, or clone the repository from the command line. Then, the resulting directory in the `mods` directory of your local Minetest installation. On Linux this is usually `~/.minetest/mods`. You do not have to ensure that the installed mod directory is named `hardtrees_redo` unless it is your personal preference to do so.

You can also install this mod in the `worldmods` folder inside any world directory to use it only within one world.

For further information or help see:
http://wiki.minetest.com/wiki/Installing_Mods
