-- hardtrees/init.lua

hardtrees_redo = {}

hardtrees_redo.modpath = minetest.get_modpath("hardtrees_redo") -- modpath
hardtrees_redo.worldpath = minetest.get_worldpath() -- worldpath
local modpath = hardtrees_redo.modpath -- modpath shortcut

minetest.register_craft( {
	output = "default:stick 1",
	recipe = {
		{ "", "", "group:leaves" },
		{ "", "group:leaves", "" },
		{ "group:leaves", "", "" }
	},
})

dofile(modpath.."/override.lua")	-- tree & leave overrides

